FROM python:3.8-slim-buster
COPY src /app/src
COPY questions/dataset-questions.csv /app/questions/dataset-questions.csv
COPY  ./requirements.txt /app/requirements.txt
WORKDIR /app
RUN pip3 install -r requirements.txt
#RUN python -m spacy download en_core_web_lg
RUN python3 -m spacy download en_core_web_sm
ENTRYPOINT ["streamlit", "run"]
CMD ["src/main.py"]
